FROM rust:latest as builder

WORKDIR r_fileserver/
COPY src/ src/.
COPY Cargo.toml .

# RUN RUSTFLAGS=-Clinker=musl-gcc cargo build --release --target=x86_64-unknown-linux-musl
RUN cargo install --path .

# FROM alpine
FROM docker.io/debian:latest
COPY --from=builder /usr/local/cargo/bin/r_fileserver /usr/local/bin/r_fileserver

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install wget

WORKDIR /usr/local/bin/
CMD ["r_fileserver"]