use dotenv::dotenv;
use actix_web::{middleware, App, HttpServer};

mod releases_service;
mod handlers;

use releases_service::ReleasesService;
use mongodb::options::ClientOptions;
use mongodb::Client;
use std::env;

pub struct Config {
    pub database_url: String,
    pub server_url: String,
    pub files_path: String,
    pub test_release: String,
    pub debug_logging: bool,
    pub mirror_name: String,
}

impl Config {
    pub fn new() -> Self {
        let database_url = env::var("DATABASE_URL").expect("DATABASE_URL is not set in .env file");
        let server_url = env::var("SERVER_URL").expect("SERVER_URL is not set in .env file");
        let mut files_path = env::var("FILES_PATH").expect("FILES_PATH is not set");
        let test_release = env::var("TEST_RELEASE_ID").expect("TEST_RELEASE_ID is not set in .env file");
        let mirror_name = env::var("MIRROR_NAME").expect("TEST_RELEASE_ID is not set in .env file");
        let debug_logging = env::var("DEBUG_LOG").is_ok();

        if files_path.chars().last().unwrap() == '/' {
            files_path.truncate(files_path.len() - 1);
        }

        Config {
            database_url,
            server_url,
            files_path,
            test_release,
            debug_logging,
            mirror_name,
        }
    }
}


pub struct ServiceManager {
    release: ReleasesService,
    config: Config,
}

impl ServiceManager {
    pub fn new(release: ReleasesService, config: Config) -> Self {
        ServiceManager { release, config }
    }
}


#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    // Load env
    dotenv().ok();

    let config = Config::new();
    if config.debug_logging {
        env::set_var("RUST_LOG", "actix_web=debug,actix_server=info");
        env_logger::init();
    }

    // Parse a connection string into an options struct.
    let client_options = ClientOptions::parse(&config.database_url).await.unwrap();
    let client = Client::with_options(client_options).unwrap();
    let db = client.database("general");
    let releases_collection = db.collection("releases");
    let stats_collection = db.collection("stats");

    println!("{}", config.server_url);

    // start server
    HttpServer::new(move || {
        let releases_service_worker = ReleasesService::new(releases_collection.clone(), stats_collection.clone());
        let service_manager = ServiceManager::new(releases_service_worker, Config::new());

        // launch http server
        App::new()
            .data(service_manager)
            .wrap(middleware::Logger::default())
            .configure(handlers::init)
    })
        .bind(config.server_url)?
        .run()
        .await
}