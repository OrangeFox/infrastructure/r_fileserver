use actix_web::{get, web, HttpResponse, Responder, Result};
use std::fs::File;
use actix_files::NamedFile;
use actix_web::http::StatusCode;
use fs2::available_space;


#[get("/status")]
async fn status(app_data: web::Data<crate::ServiceManager>) -> impl Responder {
    let release_id = &app_data.config.test_release;
    let test_release = (app_data.release.get_release(&release_id)).await;

    match test_release {
        Err(error_str) => return HttpResponse::NotAcceptable().body(format!("Self check failed! {}", error_str)),
        Ok(inner) => match inner {
            None => return HttpResponse::NotFound().body("Nothing found with such ID!"),
            _ => HttpResponse::Ok().body(format!("Self check passed! {}", &app_data.config.mirror_name))
        }
    }
}

#[get("/status/space")]
async fn status_space(app_data: web::Data<crate::ServiceManager>) -> impl Responder {
    let space = available_space(&app_data.config.files_path);
    HttpResponse::Ok().body(space.unwrap().to_string())
}


#[get("/{release_id}")]
async fn get_release(
    app_data: web::Data<crate::ServiceManager>,
    release_id: web::Path<String>,
) -> Result<NamedFile, HttpResponse> {
    let op_doc = (app_data.release.get_release(&release_id)).await;
    let doc = match op_doc {
        Err(error_str) => return Err(HttpResponse::NotFound().body(format!("Wrong release ID format! {}", error_str))),
        Ok(inner) => match inner {
            None => return Err(HttpResponse::NotFound().body("Nothing found with such ID!")),
            _ => inner.unwrap()
        }
    };

    // Release vars
    let internal_doc = doc.get_document("internal").unwrap();
    let real_filename = internal_doc.get_str("real_filename").unwrap();
    let public_filename = doc.get_str("filename").unwrap();
    let release_id = doc.get_object_id("_id").unwrap();
    let device_id = doc.get_object_id("device_id").unwrap();

    // Open file
    let path = format!("{}/{}", &app_data.config.files_path, real_filename);
    let raw_file = File::open(&path).expect("Error opening local file!");

    // Append stats
    app_data.release.update_release_stats(release_id, device_id).await.expect("Error writing stats!");

    // Return file
    Ok(NamedFile::from_file(raw_file, public_filename).unwrap())
}

// function that will be called on new Application to configure routes for this module
pub fn init(cfg: &mut web::ServiceConfig) {
    cfg.service(status);
    cfg.service(status_space);
    cfg.service(get_release);
}