use bson::{doc, Document, oid::ObjectId};
use mongodb::Collection;
use mongodb::results::UpdateResult;
use mongodb::options::UpdateOptions;
use std::env;
use chrono;


#[derive(Clone)]
pub struct ReleasesService {
    releases: Collection,
    stats: Collection,
}

impl ReleasesService {
    pub fn new(releases: Collection, stats: Collection) -> ReleasesService {
        ReleasesService { releases, stats }
    }

    pub async fn get_release(&self, release_id: &String) -> Result<Option<Document>, String> {
        let id = ObjectId::with_string(&release_id);
        if id.is_err() {
            return Err(format!("{}", id.unwrap_err()))
        };
        let result = self.releases.find_one(doc! { "_id": id.unwrap()}, None).await;
        Ok(if result.is_err() { None } else { result.unwrap() })
    }

    pub async fn update_release_stats(&self, id: &ObjectId, device_id: &ObjectId) -> mongodb::error::Result<UpdateResult> {
        let mirror_name = env::var("MIRROR_NAME").expect("MIRROR_NAME is not set");
        let mirror_string = format!("mirrors.{}", mirror_name);

        let date = chrono::Utc::now().format("%x");
        let date_string = format!("days.{}", date.to_string());

        let mut options = UpdateOptions::default();
        options.upsert = Option::from(true);

        self.stats.update_one(
            doc! { "release_id": id, "device_id": device_id },
            doc! { "$inc": { "count": 1, date_string: 1, mirror_string: 1 } },
            options
        ).await
    }
}